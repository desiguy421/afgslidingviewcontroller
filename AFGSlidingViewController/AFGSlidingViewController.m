//
//  AFGSlidingViewController.m
//  SlidingViewControllerDemo
//
//  Created by Aqeel Gunja on 2/5/13.
//  Copyright (c) 2013 Aqeel Gunja. All rights reserved.
//

#import "AFGSlidingViewController.h"

#import <objc/runtime.h>

NSString * const AFGSlidingViewControllerBottomLeftViewWillAppear = @"AFGSlidingViewControllerBottomLeftViewWillAppear";
NSString * const AFGSlidingViewControllerBottomLeftViewDidAppear = @"AFGSlidingViewControllerBottomLeftViewDidAppear";

@interface UIViewController (AFGSlidingViewControllerExtensions_Private)

- (void)afg_setSlidingViewController:(AFGSlidingViewController *)slidingViewController;

@end

@interface AFGSlidingViewController ()
<
    UIScrollViewDelegate,
    UIGestureRecognizerDelegate
>

@property (nonatomic, strong) UIView *bottomLeftContainerView;
@property (nonatomic, strong) UIView *topContainerView;

@property (nonatomic, strong) UIScrollView *hiddenScrollView;

@property (nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizer;

@end

@implementation AFGSlidingViewController
{
    BOOL _isBottomLeftViewVisible;
}

- (UITapGestureRecognizer *)tapGestureRecognizer
{
    if (!_tapGestureRecognizer)
    {
        _tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapRecognized:)];
        _tapGestureRecognizer.enabled = NO;
    }
    return _tapGestureRecognizer;
}

- (UIView *)bottomLeftContainerView
{
    if (!_bottomLeftContainerView)
    {
        CGRect frame = self.view.bounds;
        frame.size.width = [self bottomLeftWidth];
        _bottomLeftContainerView = [[UIView alloc] initWithFrame:frame];
        _bottomLeftContainerView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        _bottomLeftContainerView.backgroundColor = [UIColor clearColor];
    }
    return _bottomLeftContainerView;
}

- (UIView *)topContainerView
{
    if (!_topContainerView)
    {
        CGRect frame = self.view.bounds;
        _topContainerView = [[UIView alloc] initWithFrame:frame];
        _topContainerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _topContainerView.layer.shadowPath = [UIBezierPath bezierPathWithRect:frame].CGPath;
        _topContainerView.layer.shadowColor = [UIColor blackColor].CGColor;
        _topContainerView.layer.shadowOpacity = 0.75f;
        _topContainerView.layer.shadowRadius = 10.0f;
        _topContainerView.backgroundColor = [UIColor clearColor];
        _topContainerView.layer.shadowOffset = CGSizeMake(0, 1.0);
        //_topContainerView.alpha = 0.5;
        [_topContainerView addGestureRecognizer:self.tapGestureRecognizer];
    }
    return _topContainerView;
}

- (UIScrollView *)hiddenScrollView
{
    if (!_hiddenScrollView)
    {
        UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, [self bottomLeftWidth], 100)];
        scrollView.contentSize = CGSizeMake([self bottomLeftWidth] * 2, 100);
        scrollView.pagingEnabled = YES;
        scrollView.backgroundColor = [UIColor purpleColor];
        //scrollView.decelerationRate = UIScrollViewDecelerationRateFast;
        scrollView.hidden = YES;
        scrollView.contentOffset = CGPointMake([self bottomLeftWidth], 0);
        scrollView.bounces = self.shouldAllowBouncing;
        scrollView.delegate = self;
        //scrollView.bounces = NO;
        _hiddenScrollView = scrollView;
    }
    return _hiddenScrollView;
}

- (void)removeViewController:(UIViewController *)viewController
{
    if (viewController)
    {
        [viewController willMoveToParentViewController:nil];
        [viewController.view removeFromSuperview];
        [viewController removeFromParentViewController];
    }
}

- (void)setShouldAllowBouncing:(BOOL)shouldAllowBouncing
{
    _shouldAllowBouncing = shouldAllowBouncing;
    if (self.isViewLoaded)
    {
        self.hiddenScrollView.bounces = shouldAllowBouncing;
    }
}

- (void)setTopViewController:(UIViewController *)topViewController
{
    [_topViewController afg_setSlidingViewController:nil];
    if (self.isViewLoaded)
    {
        [self removeViewController:_topViewController];
    }
    _topViewController = topViewController;
    if (self.isViewLoaded)
    {
        [self addViewController:_topViewController toContainerView:self.topContainerView];

//        double delayInSeconds = 0.2;
//        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
//        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self setBottomLeftViewVisible:NO];
//        });
    }
    [_topViewController afg_setSlidingViewController:self];
}

- (void)addViewController:(UIViewController *)viewController toContainerView:(UIView *)containerView
{
    if (viewController)
    {
        [self addChildViewController:viewController];
        viewController.view.frame = containerView.bounds;
        [containerView insertSubview:viewController.view atIndex:0];
        [viewController didMoveToParentViewController:self];
    }
}

- (void)setBottomLeftViewController:(UIViewController *)bottomLeftViewController
{
    [_bottomLeftViewController afg_setSlidingViewController:nil];
    if (self.isViewLoaded && _isBottomLeftViewVisible)
    {
        [self removeViewController:_bottomLeftViewController];
    }
    _bottomLeftViewController = bottomLeftViewController;
    if (self.isViewLoaded && _isBottomLeftViewVisible)
    {
        [self addViewController:_bottomLeftViewController toContainerView:self.bottomLeftContainerView];
    }
    [_bottomLeftViewController afg_setSlidingViewController:self];
}

- (void)setSlidingEnabled:(BOOL)slidingEnabled
{
    _slidingEnabled = slidingEnabled;
    self.hiddenScrollView.panGestureRecognizer.enabled = _slidingEnabled;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]))
    {
        _shouldAllowBouncing = YES;
        _slidingEnabled = YES;
        _bottomLeftWidth = 276.0f;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    self.bottomLeftContainerView;
    [self.view addSubview:self.topContainerView];
    [self.view addSubview:self.hiddenScrollView];
    
    [self.topContainerView addGestureRecognizer:self.hiddenScrollView.panGestureRecognizer];
    self.hiddenScrollView.panGestureRecognizer.enabled = self.slidingEnabled;


    [self addViewController:self.topViewController toContainerView:self.topContainerView];
    //[self addViewController:self.bottomLeftViewController toContainerView:self.bottomLeftContainerView];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    if (!_isBottomLeftViewVisible)
    {
        CGRect bounds = self.view.bounds;
        CGRect leftFrame = self.bottomLeftContainerView.frame;
        leftFrame.size = CGSizeMake(leftFrame.size.width, bounds.size.height);
        self.bottomLeftContainerView.frame = leftFrame;
    }
}

- (void)updateBottomLeftState
{
    CGRect frame = self.topContainerView.frame;    
    if (frame.origin.x > 0.0)
    {
        if (!_isBottomLeftViewVisible)
        {
            NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
            [center postNotificationName:AFGSlidingViewControllerBottomLeftViewWillAppear object:self];
            
            [self.view insertSubview:self.bottomLeftContainerView atIndex:0];
            [self addViewController:self.bottomLeftViewController toContainerView:self.bottomLeftContainerView];

            [center postNotificationName:AFGSlidingViewControllerBottomLeftViewDidAppear object:self];

            self.topViewController.view.userInteractionEnabled = NO;
            self.tapGestureRecognizer.enabled = YES;
        }
        _isBottomLeftViewVisible = YES;
    }
    else
    {
        if (_isBottomLeftViewVisible)
        {
            [self removeViewController:self.bottomLeftViewController];
            [self.bottomLeftContainerView removeFromSuperview];
            self.topViewController.view.userInteractionEnabled = YES;
            self.tapGestureRecognizer.enabled = NO;
        }
        _isBottomLeftViewVisible = NO;
    }
}

- (void)setBottomLeftViewVisible:(BOOL)visible
{
    CGPoint point;
    if (!visible)
    {
        point = CGPointMake([self bottomLeftWidth], 0);
        [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^
         {
             [self.hiddenScrollView setContentOffset:point animated:NO];
         } completion:^(BOOL finished) {
             [self updateBottomLeftState];
         }];
    }
    else
    {
        point = CGPointZero;
        [self.hiddenScrollView setContentOffset:point animated:YES];
    }

}

#pragma mark - UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGPoint offset = scrollView.contentOffset;
    
    CGRect frame = self.topContainerView.frame;
    [self updateBottomLeftState];
    CGFloat xOrigin = [self bottomLeftWidth] - offset.x;
//    if (xOrigin < 0)
//    {
//        xOrigin = 0.0;
//    }
    frame.origin.x = xOrigin;

    self.topContainerView.frame = frame;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self updateBottomLeftState];
    self.bottomLeftContainerView.userInteractionEnabled = NO;
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate)
    {
        [self updateBottomLeftState];
        self.bottomLeftContainerView.userInteractionEnabled = YES;
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self updateBottomLeftState];
    self.bottomLeftContainerView.userInteractionEnabled = YES;
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    [self updateBottomLeftState];
}

#pragma mark - Gesture Recognizer Methods

- (void)tapRecognized:(UITapGestureRecognizer *)tapGesture
{
    [self setBottomLeftViewVisible:NO];
}

@end

@interface AFGReferenceContainer : NSObject

@property (nonatomic, weak) id weakObject;

@end

@implementation AFGReferenceContainer @end

@implementation UIViewController (AFGSlidingViewControllerExtensions)

static NSString *SlidingViewControllerKey = @"SlidingViewControllerKey";

- (void)afg_setSlidingViewController:(AFGSlidingViewController *)slidingViewController
{
    AFGReferenceContainer *referenceContainer = objc_getAssociatedObject(self, &SlidingViewControllerKey);
    if (!referenceContainer)
    {
        referenceContainer = [[AFGReferenceContainer alloc] init];
        objc_setAssociatedObject(self, &SlidingViewControllerKey, referenceContainer, OBJC_ASSOCIATION_RETAIN);
    }
    referenceContainer.weakObject = slidingViewController;
}

- (AFGSlidingViewController *)afg_slidingViewController
{
    UIViewController *viewController = self;
    AFGReferenceContainer *referenceContainer = objc_getAssociatedObject(viewController, &SlidingViewControllerKey);
    while (!referenceContainer)
    {
        viewController = viewController.parentViewController;
        if (!viewController)
        {
            break;
        }
        referenceContainer = objc_getAssociatedObject(viewController, &SlidingViewControllerKey);
    }
    return referenceContainer.weakObject;
}

@end
