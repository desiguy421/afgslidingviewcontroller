//
//  AFGViewController.m
//  SlidingViewControllerDemo
//
//  Created by Aqeel Gunja on 2/5/13.
//  Copyright (c) 2013 Aqeel Gunja. All rights reserved.
//

#import "AFGViewController.h"

#import "AFGSlidingViewController.h"

@interface AFGViewController ()

@end

@implementation AFGViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (IBAction)buttonTapped:(id)sender
{
    UINavigationController *controller = [[UINavigationController alloc] initWithRootViewController:[[UITableViewController alloc] init]];
    [self afg_slidingViewController].topViewController = controller;
}

@end
