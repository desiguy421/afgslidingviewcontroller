//
//  AFGAppDelegate.h
//  SlidingViewControllerDemo
//
//  Created by Aqeel Gunja on 2/5/13.
//  Copyright (c) 2013 Aqeel Gunja. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AFGSlidingViewController;

@interface AFGAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) AFGSlidingViewController *viewController;

@end
