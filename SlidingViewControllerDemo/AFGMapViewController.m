//
//  AFGMapViewController.m
//  SlidingViewControllerDemo
//
//  Created by Aqeel Gunja on 2/9/13.
//  Copyright (c) 2013 Aqeel Gunja. All rights reserved.
//

#import "AFGMapViewController.h"

#import <MapKit/MapKit.h>

@interface AFGMapViewController ()

@end

@implementation AFGMapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
