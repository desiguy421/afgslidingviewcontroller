//
//  AFGSlidingViewController.h
//  SlidingViewControllerDemo
//
//  Created by Aqeel Gunja on 2/5/13.
//  Copyright (c) 2013 Aqeel Gunja. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <QuartzCore/QuartzCore.h>

extern NSString * const AFGSlidingViewControllerBottomLeftViewWillAppear;
extern NSString * const AFGSlidingViewControllerBottomLeftViewDidAppear;


@interface AFGSlidingViewController : UIViewController

@property (nonatomic, strong) UIViewController *topViewController;

@property (nonatomic, strong) UIViewController *bottomLeftViewController;

@property (nonatomic, assign) BOOL shouldAllowBouncing;
@property (nonatomic, assign) BOOL slidingEnabled;
@property (nonatomic, assign) CGFloat bottomLeftWidth;

- (void)setBottomLeftViewVisible:(BOOL)visible;

@end

@interface UIViewController (AFGSlidingViewControllerExtensions)

- (AFGSlidingViewController *)afg_slidingViewController;

@end